#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <ctime>
#include <map>
#include "omp.h"
using namespace std;

map<string,string> verify_readings;

#define protein_total 21
map<string, int> protein_map{
	{ "Lys", 0 }, { "Asn", 0 },
	{ "Thr", 0 }, { "Arg", 0 },
	{ "Ser", 0 }, { "Ile", 0 },
	{ "Met", 0 }, { "Gln", 0 },
	{ "His", 0 }, { "Pro", 0 },
	{ "Leu", 0 }, { "Glu", 0 },
	{ "Asp", 0 }, { "Ala", 0 },
	{ "Gly", 0 }, { "Val", 0 },
	{ "Stp", 0 }, { "Tyr", 0 },
	{ "Cys", 0 }, { "Trp", 0 },
	{ "Phe", 0 }

};
map<string, string> dna_map{
	{"aaa", "Lys"}, {"aac", "Asn"}, {"aag", "Lys"}, {"aat", "Asn"},
	{"aca", "Thr"}, {"acc", "Thr"}, {"acg", "Thr"}, {"act", "Thr"},
	{"aga", "Arg"}, {"agc", "Ser"}, {"agg", "Arg"}, {"agt", "Ser"},
	{"ata", "Ile"}, {"atc", "Ile"}, {"atg", "Met"}, {"att", "Ile"},
	{"caa", "Gln"}, {"cac", "His"}, {"cag", "Gln"}, {"cat", "His"},
	{"cca", "Pro"}, {"ccc", "Pro"}, {"ccg", "Pro"}, {"cct", "Pro"},
	{"cga", "Arg"}, {"cgc", "Arg"}, {"cgg", "Arg"}, {"cgt", "Arg"},
	{"cta", "Leu"}, {"ctc", "Leu"}, {"ctg", "Leu"}, {"ctt", "Leu"},
	{"gaa", "Glu"}, {"gac", "Asp"}, {"gag", "Glu"}, {"gat", "Asp"},
	{"gca", "Ala"}, {"gcc", "Ala"}, {"gcg", "Ala"}, {"gct", "Ala"},
	{"gga", "Gly"}, {"ggc", "Gly"}, {"ggg", "Gly"}, {"ggt", "Gly"},
	{"gta", "Val"}, {"gtc", "Val"}, {"gtg", "Val"}, {"gtt", "Val"},
	{"taa", "Stp"}, {"tac", "Tyr"}, {"tag", "Stp"}, {"tat", "Tyr"},
	{"tca", "Ser"}, {"tcc", "Ser"}, {"tcg", "Ser"}, {"tct", "Ser"},
	{"tga", "Stp"}, {"tgc", "Cys"}, {"tgg", "Trp"}, {"tgt", "Cys"},
	{"tta", "Leu"}, {"ttc", "Phe"}, {"ttg", "Leu"}, {"ttt", "Phe"}
};
struct codon {
	string name;
	int count = 0;
};
void init_protein_map() {
	for (map < string, int>::iterator it = protein_map.begin(); it != protein_map.end(); it++)
		it->second = 0;
}
void function_section(string text, int start_index, int stop_index) {
	for (int i = start_index; i < stop_index; i += 3) {
		string sub = text.substr(i, 3);
#pragma omp atomic
		protein_map[dna_map[sub]]++;
	}
}
bool read_and_clean_file(string path, string* text){
	ifstream file(path);
	*text = "";
	string data;
	if (file.is_open()) {
		while (file >> data) {
			if (data == "//")
				continue;
			data.erase(remove_if(data.begin(), data.end(), [](char c) { return !isalpha(c); }), data.end());
			*text += data;
		}
		return true;
	}
	else {
		*text = "Cannot open file at path: " + path;
		return false;
	}
}
string print_protein_frequency() {
	stringstream stream;
	double count = 0, percent = 0;
	for (map<string, int>::iterator it = protein_map.begin(); it != protein_map.end(); it++) {
		count += it->second;
	}
	stream << setw(7) << "Protein" << " : " << setw(6) << "Count" << " : " << setw(10) << "Percentage" << endl;
	for (map<string, int>::iterator it = protein_map.begin(); it != protein_map.end(); it++) {
		double percentage;
		if (count == 0)
			count = 1;
		percentage = (it->second / count) * 100;
		stream << left << setw(7) << it->first << " : " << right <<  setw(6) << it->second << " : " 
			<< setw(9) << percentage << "%" << endl;
	}
	return stream.str();
}
bool verify_data() {

	string check = verify_readings["Serial run"];

	for (map<string, string>::iterator it = verify_readings.begin(); it != verify_readings.end(); it++) {
		if (it->second == check) continue;
		else {
			cout << "One or more of the readings was not being able to be verified, please try again!" << endl;
			return false;
		}
	}
	cout << "All readings have been verified against each others successfully!" << endl << endl;
	return true;
}
