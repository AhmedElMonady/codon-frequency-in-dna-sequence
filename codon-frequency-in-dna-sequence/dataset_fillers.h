#pragma once
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#define tab "\t"
#define obrace "{"
#define cbrace "}"
#define dq "\""
#define comma ", "

void fill_codon_dataset() {
	string c1, c2, c3, c4;
	int count = 0;
	ofstream hf("dna_dataset.h");
	ifstream dna("dna_codon.txt");
	hf << "#pragma once" << endl;
	hf << "#include <string>" << endl << endl;
	hf << "string codon_list[] {" << endl;
	while (dna >> c1 >> c2 >> c3 >> c4) {
		count++;
		hf << tab;
		hf << dq << c1 << dq << comma;
		hf << dq << c2 << dq << comma;
		hf << dq << c3 << dq << comma;
		if (count == (64 / 4))
			hf << dq << c4 << dq << endl;
		else

			hf << dq << c4 << dq << comma << endl;
	}
	hf << "};" << endl;
	hf << "#define codon_list_size sizeof(codon_list) / sizeof(codon_list[0])" << endl;
	hf.close();
}

void fill_codon_map() {
	ifstream codon_("dna_codon.txt");
	ifstream aminoacid("DNA_AA.txt");
	ofstream output("map_ds.h");
	stringstream stream;
	int count = 0;
	string c1, c2, c3, c4, a1, a2, a3, a4;
	stream << "#pragma once" << endl;
	stream << "#include \"utils.h\"" << endl;
	stream << "#include <map>" << endl;
	stream << endl;
	stream << "map<string,string> dna_map {" << endl;
	int countx = 0;
	while (codon_ >> c1 >> c2 >> c3 >> c4 && aminoacid >> a1 >> a2 >> a3 >> a4) {
		count++;
		countx += 4;
		stream << tab;
		stream << obrace << dq << c1 << dq << comma << dq << a1 << dq << cbrace << comma;
		stream << obrace << dq << c2 << dq << comma << dq << a2 << dq << cbrace << comma;
		stream << obrace << dq << c3 << dq << comma << dq << a3 << dq << cbrace << comma;
		if (countx == 64) {
			if (count == (64 / 4))
				stream << obrace << dq << c4 << dq << comma << dq << a4 << dq << cbrace;
			else
				stream << obrace << dq << c4 << dq << comma << dq << a4 << dq << cbrace << comma << endl;
		}
		else {
			if (count == (64 / 4))
				stream << obrace << dq << c4 << dq << comma << dq << a4 << dq << cbrace << comma;
			else
				stream << obrace << dq << c4 << dq << comma << dq << a4 << dq << cbrace << comma << endl;
		}
	}
	stream << endl;
	stream << "};" << endl;
	output << stream.str();
	output.close();
}

void fill_protein_map() {
	ifstream file("DNA_AA.txt");
	ofstream hf("header.h");
	string arr[64];
	string read;
	stringstream stream;
	int i = 0;
	while (file >> read) {
		arr[i] = read;
		i++;
	}

	for (int i = 0; i < 64; i++) {
		string a = arr[i];
		for (int j = i + 1; j < 64; j++) {
			if (a == arr[j]) arr[j] = "";
		}
	}
	int total = 0;
	for (int i = 0; i < 64; i++) {
		if (arr[i] == "") continue;
		cout << setw(4) << i + 1 << " : " << arr[i] << endl;
		stream << "{" << dq << arr[i] << dq << comma << "0" << "}" << comma << endl;
		total++;
	}
	hf << stream.str();
	hf.close();
}