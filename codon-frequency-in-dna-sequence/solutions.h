#pragma once
#include "utils.h"

double time_diff;
clock_t start_time;

void serial_execution(string text) {
	init_protein_map();
	stringstream stream;
	start_time = clock();

	for (int i = 0; i < text.length(); i += 3) {
		string sub = text.substr(i, 3);
		protein_map[dna_map[sub]]++;
	}

	time_diff = clock() - start_time;
	stream << "Time taken for serial execution is: ";
	stream << time_diff  << "ms" << endl << endl;

	cout << stream.str();
}

void parallel_for_execution(string text, int nthreads) {
	init_protein_map();
	stringstream stream;
	omp_set_num_threads(nthreads);
	start_time = clock();
#pragma omp parallel for
	for (int i = 0; i < text.length(); i += 3) {
		string sub = text.substr(i, 3);
#pragma omp atomic
		protein_map[dna_map[sub]]++;
	}

	time_diff = clock() - start_time;
	stream << "Time taken for parallel-for execution ";
	stream << "with " << setw(3) << nthreads << " threads is: ";
	stream << time_diff << "ms" << endl << endl;

	cout << stream.str();
}

void parallel_sections_execution(string text, int nthreads) {
	init_protein_map();
	stringstream stream;
	omp_set_num_threads(nthreads);
	start_time = clock();

#pragma omp parallel sections
	{
#pragma omp section
		{
			function_section(text, 0, text.length()/2);
		}
#pragma omp section
		{
			function_section(text, text.length() / 2, text.length());
		}
	}

	time_diff = clock() - start_time;
	stream << "Time taken for parallel-sections execution ";
	stream << "with "<<setw(3) << nthreads << " threads is: ";
	stream << time_diff  << "ms" << endl << endl;

	cout << stream.str();
}
