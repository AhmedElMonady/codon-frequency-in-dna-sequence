#include "solutions.h"


int main() {

	string text;
	string path = "InputSeq.dat.txt";
	bool file = read_and_clean_file(path, &text);
	
	if (file) {
		serial_execution(text);
		verify_readings["Serial run"] = print_protein_frequency();

		for (int i = 2; i <= 16; i *= 2) {
			parallel_for_execution(text, i);
			verify_readings["For run with" + i] = print_protein_frequency();
		}
		
		for (int i = 2; i <= 16; i *= 2) {
			parallel_sections_execution(text, i);
			verify_readings["Sections run with" + i] = print_protein_frequency();
		}

		if (verify_data())
			cout << print_protein_frequency() << endl;
	}
	else cout << text << endl;
	
}
